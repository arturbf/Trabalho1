#include "../include/smooth.hpp"
#include <iostream>
#include <stdlib.h>
#include <vector>

using namespace std;

Smooth::Smooth(){

}

//Usando o algoritimo para aplicar o filtro smooth
void Smooth::aplicarFiltro(Imagem &imagem){
	int i, j, value;


	for(i=1; i<imagem.getLinhas()-1; i++)
	{
		for(j=1; j<imagem.getColunas()-1; j++) 
		{
			
			value = 0;

			value = (imagem.getPixel((i+1), j) + imagem.getPixel((i+1), (j+1)) + imagem.getPixel((i+1), (j-1)) + imagem.getPixel((i-1), j) + imagem.getPixel((i-1), (j+1)) + imagem.getPixel((i-1), (j-1)) + imagem.getPixel(i, (j+1)) + imagem.getPixel(i, (j-1)))/8;

			value = value < 0 ? 0 : value;
			value = value > 255 ? 255 : value;

				imagem.mudarPixel(i, j, (int)value);
		}
	}

    cout << "Fase de aplicacao de filtro smooth concluida!!" << endl;
}
