#include "../include/imagem.hpp"
#include "../include/filtro.hpp"
#include "../include/negativo.hpp"
#include "../include/smooth.hpp"
#include "../include/sharpen.hpp"

using namespace std;

int main(int argc, char *argv[]){
	
	//Criando Objeto para aplicar o filtro negativo		
	Imagem Lena_Negativo(512, 512, 255);
	Lena_Negativo.lerImagem("../lena.pgm");
	Lena_Negativo.salvarImagem("../copia.pgm");
	Negativo filtro_Negativo;
	filtro_Negativo.aplicarFiltro(Lena_Negativo);
	Lena_Negativo.salvarImagem("../negativo.pgm");

	//Criando Objeto para aplicar o filtro smooth
	Imagem Lena_Smooth(512, 512, 255);
	Lena_Smooth.lerImagem("../lena.pgm");
	Smooth filtroSmooth;
	filtroSmooth.aplicarFiltro(Lena_Smooth);
	Lena_Smooth.salvarImagem("../smooth.pgm");

	//Criando Objeto para aplicar o filtro sharpen	
	Imagem Lena_Sharpen(512, 512, 255);
	Lena_Sharpen.lerImagem("../lena.pgm");
	Sharpen filtro_Sharpen;
	filtro_Sharpen.setDiv(1);
	filtro_Sharpen.setSize(3);
	filtro_Sharpen.aplicarFiltro(Lena_Sharpen, filtro_Sharpen.getDiv(), filtro_Sharpen.getSize());
	Lena_Sharpen.salvarImagem("../sharpen.pgm");

	return 0;
}
