Para que o usuario consiga fazer com que progama aplique os filtros: 
Abra a pasta "bin" e entre no "Binario" para que ele possa ser executado.

Após o programa ser executado, será criado 4 arquivos em pgm.
3 denominados com o filtro "aplicado.pgm" (exemplo: negativo.pgm).
1 um com a copia da imagem (copia.pgm).
Todos eles foram armazenados na pasta "Imagens_Resultantes".

Para remover todo os arquivos .o e o binario:
Abra o terminal e de o comando:
$make clean

Para compilar o codigo:
$make
