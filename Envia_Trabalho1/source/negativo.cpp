#include "../include/negativo.hpp"
#include "../include/imagem.hpp"
#include <iostream>

using namespace std;

Negativo::Negativo(){

}
//Usando o algoritimo para aplicar o filtro negativo
void Negativo::aplicarFiltro(Imagem &imagem){
	int i, j;

	for(i=0; i<imagem.getLinhas(); i++)
    {
        for(j=0; j<imagem.getColunas(); j++) 
        {
        	int novoValor = 255 - imagem.getPixel(i, j);
			imagem.mudarPixel(i, j, novoValor);
        }
    }

    cout << "Fase de aplicacao de filtro negativo concluida!!" << endl;
}
