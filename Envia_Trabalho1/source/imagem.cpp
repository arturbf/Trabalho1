#include "../include/imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

Imagem::Imagem() {
	Linhas = 0;
	Colunas = 0;
	Cinza = 0;
	matriz = NULL;
}

Imagem::Imagem(int Linhas, int Colunas, int Cinza) {
	this->Linhas = Linhas;
	this->Colunas = Colunas;
	this->Cinza = Cinza;
	matriz = new int[Linhas*Colunas];
}

int Imagem::getCinza() {
	return Cinza;
}

int Imagem::getLinhas() {
	return Linhas;	
}

int Imagem::getColunas() {
	return Colunas;
}

Imagem::~Imagem(){
	delete []matriz;
}
void Imagem::lerImagem(const char fname[]) {
	int i, j;
    unsigned char *charImage;
    char header [100], *ptr;
    ifstream ifp;

    ifp.open(fname, ios::in | ios::binary);

    if (!ifp) 
    {
        cout << "Nao e possivel ler a imagem: " << fname << endl;
        exit(1);
    }


    ifp.getline(header,100,'\n');
    if ( (header[0]!=80) || (header[1]!=53) )
    {   
        cout << "Imagem " << fname << " nao e PGM" << endl;
        exit(1);
    }

    ifp.getline(header,100,'\n');
    while(header[0]=='#')
        ifp.getline(header,100,'\n');

    Linhas=strtol(header,&ptr,0);
    Colunas=atoi(ptr);

    ifp.getline(header,100,'\n');
    Cinza=strtol(header,&ptr,0);

    charImage = (unsigned char *) new unsigned char [Linhas*Colunas];

    ifp.read( reinterpret_cast<char *>(charImage), (Linhas*Colunas)*sizeof(unsigned char));

    if (ifp.fail()) 
    {
        cout << "Imagem " << fname << " esta com o tamanho errado" << endl;
        exit(1);
    }

    ifp.close();


    int val;

    for(i=0; i<Linhas; i++)
        for(j=0; j<Colunas; j++) 
        {
            val = (int)charImage[i*Colunas+j];
            matriz[i*Colunas+j] = val;   
        }

    delete [] charImage;
}

void Imagem::salvarImagem(const char filename[]) {
	int i, j;
	unsigned char *charImage;
	ofstream outfile(filename);
	
	charImage = (unsigned char *) new unsigned char [Linhas*Colunas];
	
	int val;
	
    for(i=0; i<Linhas; i++)
    {
        for(j=0; j<Colunas; j++) 
        {
			
			val = matriz[i*Colunas+j];
			charImage[i*Colunas+j]=(unsigned char)val;
        }
    }
    
	if (!outfile.is_open())
	{
		cout << "Nao e possivel abrir o arquivo de saida"  << filename << endl;
		exit(1);
	}
	
	outfile << "P5" << endl;
    outfile << Linhas << " " << Colunas << endl;
    outfile << 255 << endl;
	
	outfile.write(reinterpret_cast<char *>(charImage), (Linhas*Colunas)*sizeof(unsigned char));
	
	cout << "Arquivo " << filename << " criado com sucesso!" << endl;
	outfile.close();
}

void Imagem::mudarPixel(int i, int j, int novoValor){
    matriz[i*Colunas+j] = novoValor;
}

int Imagem::getPixel(int i, int j){
    return matriz[i*Colunas + j];
}
