#ifndef NEGATIVO_H
#define NEGATIVO_H

#include "filtro.hpp"


class Negativo : public Filtro{
public:
	
	Negativo();

	void aplicarFiltro(Imagem &imagem);

};

#endif
