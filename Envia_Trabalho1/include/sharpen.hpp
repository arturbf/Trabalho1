#ifndef SHARPEN_H
#define SHARPEN_H
#include "filtro.hpp"

class Sharpen : public Filtro{

	private:

	int div;
	int size;

public:

	Sharpen();
	void setDiv(int div);
	int getDiv();
	void setSize(int size);
	int getSize();

	void aplicarFiltro(Imagem &imagem, int div, int size);

};

#endif
