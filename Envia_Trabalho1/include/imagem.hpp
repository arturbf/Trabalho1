#ifndef _IMAGEM_H_
#define _IMAGEM_H_

using namespace std;

class Imagem
{
	
	private:
		//Atributos
		int Linhas;
		int Colunas;
		int Cinza;
		int *matriz;

	public:
		//Construtor regulador de variaveis
		Imagem();
		//Construtor de sobrecarga
		Imagem(int Linhas, int Colunas, int Cinza);

		//Acessadores de funçao ~ irá retornar algo
		int getCinza();		
		int getLinhas();
		int getColunas();
		int getPixel(int i, int j);

		//Destrutor
		~Imagem();

		//Métodos de manipulação de imagem, os nomes são auto-explicativos
		void lerImagem(const char fname[]);
		void salvarImagem(const char filename[]);
		void mudarPixel(int i, int j, int novoValor);
		
};

#endif	
